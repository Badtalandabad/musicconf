let express = require('express');
let router = express.Router();
let tracksController = require('../core/controllers/tracksController');

router.get('/', tracksController.showTracks);
router.get('/show/:trackId', tracksController.showTrack);

router.get('/add', (req, res) => {
  tracksController.editTrack(req, res, 'add');
});
router.get('/edit', (req, res) => {
  tracksController.editTrack(req, res, 'edit');
});
router.post('/save', tracksController.saveTrack);

module.exports = router;
