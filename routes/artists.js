let express = require('express');
let router = express.Router();
let artistsController = require('../core/controllers/artistsController');

router.get('/', artistsController.showArtists);
router.get('/show/:artistId', artistsController.showArtist);

router.get('/add', (req, res) => {
  artistsController.editArtist(req, res, 'add');
});
router.get('/edit', (req, res) => {
  artistsController.editArtist(req, res, 'edit');
});
router.get('/save', artistsController.saveArtist);

module.exports = router;
