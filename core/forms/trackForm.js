module.exports = {
  trackFullName: 'trackFullName',
  trackDescription: 'trackDescription',
  trackPublished: 'published',
  trackArtists: 'trackArtists',
  trackChannels: 'trackChannels',
};