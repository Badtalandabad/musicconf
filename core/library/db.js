let mysql = require('mysql');
let config = require('../../config/config').db;

/**
 * Database handler
 *
 * @type {{connect: dbHandler.connect}}
 */
let dbHandler = {
  /**
   * Connects to database and executes callback on success
   *
   * @param onSuccessCallback Callback to execute on successful connection
   */
  connect: function(onSuccessCallback) {
    let dbConnection = mysql.createConnection({
      host: config.host,
      user: config.user,
      password: config.password,
      database: config.database,
      charset: config.charset,
    });

    dbConnection.connect((err) => {
      if (err) throw err;
      onSuccessCallback(dbConnection);
    });
  },
};

module.exports = dbHandler;