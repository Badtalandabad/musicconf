let dispatcher = {
  dispatch: function(responseObject, template, params = {}) {
    responseObject.render(template, params, (err, contentHtml) => {
      if (err) throw err;
      params._content = contentHtml;
      responseObject.render('layout', params);
    });
  }
};

module.exports = dispatcher;
