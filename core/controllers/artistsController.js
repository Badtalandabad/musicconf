let artistsModel = require('../models/artistsModel');
let dispatcher = require('../library/dispatcher');

/**
 * Artists controller
 */
let artistsController = {
  /**
   * Retrieves artists from database and shows them to user
   *
   * @param req Express request object
   * @param res Express response object
   */
  showArtists: function (req, res) {
    let displayArtists = (queryResult) => {
      let artists = [];
      queryResult.forEach((item, index) => {
        let artistId = item.id;

        if (!artists[artistId]) {
          artists[artistId] = {
            id: item.id,
            publicName: item.public_name,
            channels: []
          };
        }
        // add channel
        if (!artists[artistId].channels[item.channel_id]) {
          let artistUrl = item.channel_url_pattern.replace('{base_url}', item.supplier_base_url);
          artistUrl = artistUrl.replace('{channel_id}', item.channel_id_on_supplier_side);
          artistUrl = 'https://' + artistUrl;
          let channel = {
            id: item.channel_id,
            name: item.channel_name,
            supplierName: item.supplier_name,
            artistUrl: artistUrl,
            supplierCode: item.supplier_code
          };
          artists[artistId].channels[channel.id] = channel;
        }
      });

      dispatcher.dispatch(res, 'artists/artistsList', {title: 'Artists', artists: artists});
    };

    artistsModel.getArtistsWithChannelLinks(displayArtists);
  },

  /**
   * Retrieves artist from database and shows it to user
   *
   * @param req Express request object
   * @param res Express response object
   */
  showArtist: function (req, res) {
    let artistId = parseInt(req.params.artistId);
    if (!artistId) throw Error('Wrong argument in artist view');

    let displayArtists = (queryResult) => {
      let artist = {
        id: queryResult[0] ? queryResult[0].id : null,
        publicName: queryResult[0] ? queryResult[0].public_name : null,
        description: queryResult[0] ? queryResult[0].description : null,
        channels: [],
      };

      queryResult.forEach((item, index) => {
        // add channel
        if (!artist.channels[item.channel_id]) {
          let artistUrl = item.channel_url_pattern.replace('{base_url}', item.supplier_base_url);
          artistUrl = artistUrl.replace('{channel_id}', item.channel_id_on_supplier_side);
          artistUrl = 'https://' + artistUrl;
          let channel = {
            id: item.channel_id,
            name: item.channel_name,
            supplierName: item.supplier_name,
            artistUrl: artistUrl,
            supplierCode: item.supplier_code
          };
          artist.channels[channel.id] = channel;
        }
      });

      dispatcher.dispatch(res, 'artists/artist', {title: 'Artist: ' + artist.publicName, artist: artist});
    };

    artistsModel.getArtistWithChannelLinks(artistId, displayArtists);
  },

  /**
   * Shows edit track page
   *
   * @param req  Express request object
   * @param res  Express response object
   * @param type "add" or "edit" artist
   */
  editArtist: function (req, res, type) {
    let titleType = type.charAt(0).toUpperCase() + type.slice(1);
    dispatcher.dispatch(res, 'artists/editArtist', {title: titleType + ' artist', type: type});
  },

  saveArtist: function (req, res) {

  },

};

module.exports = artistsController;