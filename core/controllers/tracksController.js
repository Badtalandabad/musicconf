let tracksModel = require('../models/tracksModel');
let artistsModel = require('../models/artistsModel');
let dispatcher = require('../library/dispatcher');
let trackForm = require('../forms/trackForm');

/**
 * Tracks controller
 */
let tracksController = {
  /**
   * Retrieves tracks from database and shows them to user
   *
   * @param req Express request object
   * @param res express response object
   */
  showTracks: function (req, res) {
    tracksModel.getTracks((queryResult) => {
      if (!Array.isArray(queryResult)) throw Error('Wrong tracks query result');

      let tracks = [];
      queryResult.forEach((item, index) => {
        let trackId = item.id;
        // track - artists[]
        //      |- channels[]
        // add track
        if (!tracks[trackId]) {
          tracks[trackId] = {
            id: item.id,
            fullName: item.full_name,
            artists: [],
            channels: [],
          };
        }
        // add artist
        if (item.artist_id && !tracks[trackId].artists[item.artist_id]) {
          let artist = {
            id: item.artist_id,
            publicName: item.artist_public_name,
          };
          tracks[trackId].artists[artist.id] = artist;
        }
        // add channel
        if (item.channel_id && !tracks[trackId].channels[item.channel_id]) {
          let trackUrl = item.track_url_pattern.replace('{base_url}', item.supplier_base_url);
          trackUrl = trackUrl.replace('{channel_id}', item.channel_id_on_supplier_side);
          trackUrl = trackUrl.replace('{track_id}', item.track_id_on_supplier_side);
          trackUrl = 'https://' + trackUrl;
          let channel = {
            id: item.channel_id,
            name: item.channel_name,
            supplierName: item.supplier_name,
            trackUrl: trackUrl,
            supplierCode: item.supplier_code,
          };
          tracks[trackId].channels[channel.id] = channel;
        }
      });

      dispatcher.dispatch(res, 'tracks/tracksList', {title: 'Tracks', tracks: tracks});
    });
  },

  /**
   * Retrieves track from database and shows it to user
   *
   * @param req Express request object
   * @param res Express response object
   */
  showTrack: function (req, res) {
    let trackId = parseInt(req.params.trackId);
    if (!trackId) throw Error('Wrong argument in track view');

    let displayTrack = (queryResult) => {
      if (!Array.isArray(queryResult)) throw Error('Wrong track query result');

      let track = {
        id: queryResult[0] ? queryResult[0].id : null,
        fullName: queryResult[0] ? queryResult[0].full_name : null,
        description: queryResult[0] ? queryResult[0].description : null,
        artists: [],
        channels: [],
      };
      // track - artists[]
      //      |- channels[]
      // add track

      queryResult.forEach((item, index) => {
        // add artist
        if (item.artist_id && !track.artists[item.artist_id]) {
          let artist = {
            id: item.artist_id,
            publicName: item.artist_public_name
          };
          track.artists[artist.id] = artist;
        }
        // add channel
        if (item.channel_id && !track.channels[item.channel_id]) {
          let trackUrl = item.track_url_pattern.replace('{base_url}', item.supplier_base_url);
          trackUrl = trackUrl.replace('{channel_id}', item.channel_id_on_supplier_side);
          trackUrl = trackUrl.replace('{track_id}', item.track_id_on_supplier_side);
          trackUrl = 'https://' + trackUrl;
          let channel = {
            id: item.channel_id,
            name: item.channel_name,
            supplierName: item.supplier_name,
            trackUrl: trackUrl,
            supplierCode: item.supplier_code
          };
          track.channels[channel.id] = channel;
        }
      });
      dispatcher.dispatch(res, 'tracks/track', {title: 'Track: ' + track.fullName, track: track});
    };
    tracksModel.getTrack(trackId, displayTrack);
  },

  /**
   * Shows edit track page
   *
   * @param req Express request object
   * @param res Express response object
   * @param type "add" or "edit" track
   */
  editTrack: function (req, res, type) {
    let titleType = type.charAt(0).toUpperCase() + type.slice(1);

    artistsModel.getArtists((artistsQueryResult) => {
      artistsModel.getChannelsWithArtistsSuppliersNames((channelsQueryResult) => {
        let artists = artistsQueryResult.map((item) => {
          item.publicName = item.public_name;
          delete item.public_name;
          return item;
        });
        let channels = channelsQueryResult.map((item) => {
          item.supplierName = item.supplier_name;
          delete item.supplier_name;
          item.supplierCode = item.supplier_code;
          delete item.supplier_code;
          return item;
        });
        let params = {
          title: titleType + ' track',
          type: type,
          artists: artists,
          channels: channels,
        };
        dispatcher.dispatch(res, 'tracks/editTrack', params);
      });
    });
  },

  saveTrack: function (req, res) {
    let result = {
      success: false,
      errors: [],
    };

    let track = {
      fullName: req.body.trackFullName,
      description: req.body.trackDescription ? req.body.trackDescription : null,
      published: req.body.trackPublished ? new Date(req.body.trackPublished) : null,
      artists: Array.isArray(req.body.trackArtists) ? req.body.trackArtists : [],
      channels: Array.isArray(req.body.trackChannels) ? req.body.trackChannels : [],
    };

    if (!track.fullName) {
      result.errors.push({
        field: trackForm.trackFullName,
        message: 'Track full name should consist of at least 1 character',
      });
    }
    if (track.fullName.length > 255) {
      result.errors.push({
        field: trackForm.trackFullName,
        message: 'Track full name should be no more than 255 characters long',
      });
    }
    if (track.description) {
      //optional field
      if (track.description.length > 1000) {
        result.errors.push({
          field: trackForm.trackDescription,
          message: 'Description should be no more than 1000 characters long',
        });
      }
    }
    if (track.published) {
      //optional field
      if (isNaN(track.published.getTime())) {
        result.errors.push({
          field: trackForm.trackPublished,
          message: 'Invalid date',
        });
      }
      if (track.published > new Date()) {
        result.errors.push({
          field: trackForm.trackPublished,
          message: 'Published date cannot be in future',
        });
      }
    }
    if (track.artists.length === 0) {
      result.errors.push({
        field: trackForm.trackArtists,
        message: 'Choose at lease one artist',
      });
    }
    // Invalid artists and channels checks

    if (result.errors.length === 0) {
      tracksModel.saveTrack(track, () => {
        res.redirect('/tracks');
      });
    } else {
      res.redirect('add');
    }
  },

};

module.exports = tracksController;