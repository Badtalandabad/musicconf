let db = require('../library/db');

/**
 * Tracks model
 *
 * @type {{getTracks: tracksModel.getTracks}}
 */
let tracksModel = {
  /**
   * Retrieves all tracks from database
   *
   * @param onSuccessCallback
   */
  getTracks: function (onSuccessCallback) {
    let executeQuery = (dbConnection) => {
      let query = 'SELECT tracks.*, artists.id AS artist_id, artists.public_name AS artist_public_name,'
        + ' channels.id AS channel_id, channels.name AS channel_name,'
        + ' channels.id_on_supplier_side AS channel_id_on_supplier_side, ct_map.track_id_on_supplier_side,'
        + ' suppliers.name AS supplier_name, suppliers.base_url AS supplier_base_url, suppliers.track_url_pattern,'
        + ' suppliers.code AS supplier_code'
        + ' FROM tracks'
        + ' LEFT JOIN tracks_artists_map AS ta_map ON ta_map.track_id = tracks.id'
        + ' LEFT JOIN artists ON ta_map.artist_id = artists.id'
        + ' LEFT JOIN channels_tracks_map AS ct_map ON ct_map.track_id = tracks.id'
        + ' LEFT JOIN channels ON ct_map.channel_id = channels.id'
        + ' LEFT JOIN suppliers ON channels.supplier_id = suppliers.id';

      dbConnection.query(query, (err, queryResult) => {
        dbConnection.end();
        if (err) throw err;
        onSuccessCallback(queryResult);
      });
    };

    db.connect(executeQuery);
  },

  /**
   * Retrieves track with given id from database
   *
   * @param trackId           Track id
   * @param onSuccessCallback Callback on success
   */
  getTrack: function (trackId, onSuccessCallback) {
    trackId = Number(trackId);
    if (!Number.isInteger(trackId)) throw Error('Wrong argument in getTrack');

    let executeQuery = (dbConnection) => {
      let query = 'SELECT tracks.*, artists.id AS artist_id, artists.public_name AS artist_public_name,'
        + ' channels.id AS channel_id, channels.name AS channel_name,'
        + ' channels.id_on_supplier_side AS channel_id_on_supplier_side, ct_map.track_id_on_supplier_side,'
        + ' suppliers.name AS supplier_name, suppliers.base_url AS supplier_base_url, suppliers.track_url_pattern,'
        + ' suppliers.code AS supplier_code'
        + ' FROM tracks'
        + ' LEFT JOIN tracks_artists_map AS ta_map ON ta_map.track_id = tracks.id'
        + ' LEFT JOIN artists ON ta_map.artist_id = artists.id'
        + ' LEFT JOIN channels_tracks_map AS ct_map ON ct_map.track_id = tracks.id'
        + ' LEFT JOIN channels ON ct_map.channel_id = channels.id'
        + ' LEFT JOIN suppliers ON channels.supplier_id = suppliers.id'
        + ' WHERE tracks.id = ?';

      dbConnection.query(query, [trackId], (err, queryResult) => {
        dbConnection.end();
        if (err) throw err;
        onSuccessCallback(queryResult);
      });
    };

    db.connect(executeQuery);
  },

  /**
   * Saves track in database
   *
   * @param track
   * @param onSuccessCallback
   */
  saveTrack: function (track, onSuccessCallback) {
    if (!track.fullName) throw Error('Required field track full name is missing');
    db.connect((dbConnection) => {
      let query = track.id ? 'UPDATE' : 'INSERT INTO';
      query += ' tracks SET ';

      let sets = ['full_name = ?',];
      let values = [track.fullName];
      if (track.name) {
        sets.push('name = ?');
        values.push(track.name);
      }
      if (track.description) {
        sets.push('description = ?');
        values.push(track.description);
      }
      if (track.published) {
        sets.push('published = ?');
        values.push(track.published.getFullYear()
          + '-' + track.published.getMonth()
          + '-' + track.published.getDate());
      }

      query += sets.join(', ');
      if (track.id) {
        query += 'WHERE id = ?';
        values.push(track.id);
      }

      dbConnection.query(query, values, (err, queryResult) => {
        dbConnection.end();
        if (err) throw err;
        onSuccessCallback(queryResult);
      });
    });
  }
};

module.exports = tracksModel;