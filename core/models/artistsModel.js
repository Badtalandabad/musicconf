let db = require('../library/db');

/**
 * Artists model
 *
 * @type {{getArtists: artistsModel.getArtists}}
 */
let artistsModel = {
  /**
   * Retrieves all artists
   *
   * @param onSuccessCallback
   */
  getArtists: function (onSuccessCallback) {
    let executeQuery = (dbConnection) => {
      let query = 'SELECT * FROM artists';

      dbConnection.query(query, (err, queryResult) => {
        dbConnection.end();
        if (err) throw err;
        if (!Array.isArray(queryResult)) throw Error('Wrong artists query result');
        onSuccessCallback(queryResult);
      });
    };

    db.connect(executeQuery);
  },

  /**
   * Retrieves all channels
   *
   * @param onSuccessCallback
   */
  getChannelsWithArtistsSuppliersNames: function (onSuccessCallback) {
    let executeQuery = (dbConnection) => {
      let query = 'SELECT channels.*, artists.public_name AS artist_public_name,'
          + ' suppliers.name AS supplier_name, suppliers.code AS supplier_code'
        + ' FROM channels'
        + ' LEFT JOIN artists ON artists.id = channels.artist_id'
        + ' LEFT JOIN suppliers on channels.supplier_id = suppliers.id';

      dbConnection.query(query, (err, queryResult) => {
        dbConnection.end();
        if (err) throw err;
        if (!Array.isArray(queryResult)) throw Error('Wrong artists query result');
        onSuccessCallback(queryResult);
      });
    };

    db.connect(executeQuery);
  },

  /**
   * Retrieves all artists from database
   *
   * @param onSuccessCallback Callback with query result as an argument
   */
  getArtistsWithChannelLinks: function (onSuccessCallback) {
    let executeQuery = (dbConnection) => {
      let query = 'SELECT artists.*, channels.id AS channel_id, channels.supplier_id, channels.name AS channel_name,'
        + ' channels.id_on_supplier_side AS channel_id_on_supplier_side, suppliers.name AS supplier_name,'
        + ' suppliers.base_url AS supplier_base_url, suppliers.channel_url_pattern, suppliers.code AS supplier_code'
        + ' FROM artists'
        + ' LEFT JOIN channels ON channels.artist_id = artists.id'
        + ' LEFT JOIN suppliers ON channels.supplier_id = suppliers.id';

      dbConnection.query(query, (err, queryResult) => {
        dbConnection.end();
        if (err) throw err;
        if (!Array.isArray(queryResult)) throw Error('Wrong artists/channels query result');
        onSuccessCallback(queryResult);
      });
    };

    db.connect(executeQuery);
  },

  /**
   * Retrieves artist from database
   *
   * @param artistId
   * @param onSuccessCallback
   */
  getArtistWithChannelLinks: function (artistId, onSuccessCallback) {
    artistId = Number(artistId);
    if (!Number.isInteger(artistId)) throw Error('Wrong argument in getArtist');

    let executeQuery = (dbConnection) => {
      let query = 'SELECT artists.*, channels.id AS channel_id, channels.supplier_id, channels.name AS channel_name,'
        + ' channels.id_on_supplier_side AS channel_id_on_supplier_side, suppliers.name AS supplier_name,'
        + ' suppliers.base_url AS supplier_base_url, suppliers.channel_url_pattern, suppliers.code AS supplier_code'
        + ' FROM artists'
        + ' LEFT JOIN channels ON channels.artist_id = artists.id'
        + ' LEFT JOIN suppliers ON channels.supplier_id = suppliers.id'
        + ' WHERE artists.id = ?';

      dbConnection.query(query, [artistId], (err, queryResult) => {
        dbConnection.end();
        if (err) throw err;
        if (!Array.isArray(queryResult)) throw Error('Wrong artist/channels query result');
        onSuccessCallback(queryResult);
      });
    };

    db.connect(executeQuery);
  },
};

module.exports = artistsModel;