const app = {
  location: {
    controller: null,
    action: null,
  },

  init: function () {

  },

};

document.addEventListener("DOMContentLoaded", function () {
  let initPublishedField = () => {
    let date = new Date();
    let month = date.getMonth() + 1;
    let stringDate = date.getFullYear() + '-' + (month < 10 ? '0' + month : month) + '-' + date.getDate();

    document.getElementById('track-published')
      .setAttribute('max', stringDate);
  };
  let initArtistSelector = () => {
    let selectClick = function (event) {
      if (this.selectedIndex) {
        let artistName = this.options[this.selectedIndex].innerText;
        let hyphenPos = artistName.indexOf('-');
        artistName = hyphenPos > -1 ? artistName.slice(0, hyphenPos) : artistName;
        artistName = artistName.trim();

        let supplierCode = this.options[this.selectedIndex].getAttribute('data-supplier-code');
        let supplierName = this.options[this.selectedIndex].getAttribute('data-supplier-name');

        let removeButton = document.createElement('a');
        removeButton.href = '#';
        removeButton.onclick = function (event) {
          this.parentNode.remove();
          return false;
        };
        removeButton.appendChild(document.createTextNode('[X]'));

        let logoImage = document.createElement('img');
        logoImage.className = 'b-link-logo';
        logoImage.src = '/images/logos/supplier-logo-' + supplierCode + '.svg';
        logoImage.alt = supplierName;

        let inputName = this.getAttribute('data-hidden-input-name');
        let hiddenArtistId = document.createElement('input');
        hiddenArtistId.type = 'hidden';
        hiddenArtistId.name = inputName;
        hiddenArtistId.value = this.value;

        let artistTag = document.createElement('span');
        artistTag.className = 'b-form-tag';
        artistTag.appendChild(logoImage);
        artistTag.appendChild(document.createTextNode(' ' + artistName + ' '));
        artistTag.appendChild(removeButton);
        artistTag.appendChild(hiddenArtistId);

        document.getElementById(this.id + '-box').appendChild(artistTag);
      }
    };

    document.getElementById('track-artists-select').onchange = selectClick;
    document.getElementById('track-channels-select').onchange = selectClick;
  };

  if (location.pathname.indexOf('/tracks/add') === 0
    || location.pathname.indexOf('/tracks/edit/') === 0
  ) {
    initPublishedField();
    initArtistSelector();
  }
});

